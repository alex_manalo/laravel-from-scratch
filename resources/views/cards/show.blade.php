@extends('layout')

    @section('content')
            
        <h1>{{ $card->title }}</h1>        

        <ul>
        	@foreach($card->notes as $note)

        		<li>
        			<span>{{ $note->user->name}}</span> 
        				{{ $note->body }} 
        			<span><a href="/notes/{{ $note->id}}/edit">Edit</a></span>
        		</li>

        	@endforeach

        </ul>

        <h3>Add a new note</h3>        

        <form method="POST" action="/cards/{{ $card->id }}/notes">
        	
        	{!! csrf_field() !!}

        	<textarea name="body">{{ old('body') }}</textarea>

        	<input type="submit" value="Submit">
        </form>

        @foreach($errors->all() as $error)

        	<li>{{ $error }}</li>

        @endforeach

        

    @stop        

